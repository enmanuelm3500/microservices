package com.microservicios.cursos.service.impl;

import com.microservicios.commos.service.impl.CommonServiceImpl;
import com.microservicios.cursos.clients.RespuestasFeignClients;
import com.microservicios.cursos.entity.Curso;
import com.microservicios.cursos.repository.CursoRepository;
import com.microservicios.cursos.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoService {

    @Autowired
    private RespuestasFeignClients clients;

    @Override
    @Transactional(readOnly = true)
    public Curso findCursoByAlumnosId(Long id) {
        return repository.findCursoByAlumnosId(id);
    }

    @Override
    public Iterable<Long> BusquedaExamenesByAlumno(Long id) {
        return clients.findExamenesByAlumno(id);
    }
}
