package com.microservicios.cursos.service;

import com.microservicios.commos.service.CommonService;
import com.microservicios.cursos.entity.Curso;

public interface CursoService extends CommonService<Curso> {

    Curso findCursoByAlumnosId(Long id);

    Iterable<Long> BusquedaExamenesByAlumno(Long id);

}
