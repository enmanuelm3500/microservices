package com.microservicios.cursos.controller;


import com.microservices.common.examen.entity.Examen;
import com.microservicios.common.alumno.alumno.entity.Alumno;
import com.microservicios.commos.controller.CommonController;
import com.microservicios.cursos.entity.Curso;
import com.microservicios.cursos.service.CursoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class CursoController extends CommonController<Curso, CursoService> {

    @PutMapping("/asignar-alumno")
    public ResponseEntity<Curso> addAlumno(@RequestBody Curso curso){
        Optional<Curso> cursoDb = this.service.findById(curso.getId());
        if(!cursoDb.isPresent()){
            return buildResponseEntityNoContent();
        }
        curso.getAlumnos().forEach(alumno -> {
            cursoDb.get().addAlumnos(alumno);
        });
        return createOkStatusResponseEntity(service.save(cursoDb.get()));
    }


    @PutMapping("/{id}/eliminar-alumno")
    public ResponseEntity<Curso> removeAlumno(@RequestBody Alumno alumno, @PathVariable Long id){
        Optional<Curso> cursoDb = this.service.findById(id);
        if(!cursoDb.isPresent()){
            return buildResponseEntityNoContent();
        }
            cursoDb.get().removeAlumnos(alumno);
        return createOkStatusResponseEntity(service.save(cursoDb.get()));
    }

    @GetMapping("/alumno/{id}")
    public ResponseEntity<Alumno> buscarPorId(@PathVariable Long id){
         Curso curso = service.findCursoByAlumnosId(id);
         if (curso != null) {
             List<Long> examenesId = (List<Long>) service.BusquedaExamenesByAlumno(id);

             List<Examen> examenes = curso.getExamenes().stream().map(examen -> {
                 if (examenesId.contains(examen.getId())) {
                     examen.setRespondido(true);
                 }
                 return examen;
             }).collect(Collectors.toList());
             curso.setExamenes(examenes);
         }
        return createOkStatusResponseEntity(curso);
    }

    @PutMapping("/asignar-examen")
    public ResponseEntity<Curso> addExamen(@RequestBody Curso curso){
        Optional<Curso> cursoDb = this.service.findById(curso.getId());
        if(!cursoDb.isPresent()){
            return buildResponseEntityNoContent();
        }
        curso.getExamenes().forEach(examen -> {
            cursoDb.get().addExamen(examen);
        });
        return createOkStatusResponseEntity(service.save(cursoDb.get()));
    }


    @PutMapping("/{id}/eliminar-examen")
    public ResponseEntity<Curso> removeAlumno(@RequestBody Examen examen, @PathVariable Long id){
        Optional<Curso> cursoDb = this.service.findById(id);
        if(!cursoDb.isPresent()){
            return buildResponseEntityNoContent();
        }
        cursoDb.get().removeExamen(examen);
        return createOkStatusResponseEntity(service.save(cursoDb.get()));
    }

}
