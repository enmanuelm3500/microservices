package com.microservicios.commos.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.OK;

public abstract class AbstractController {

    protected ResponseEntity createOkStatusResponseEntity(Object body) {
        return buildResponseEntity(body, OK);
    }

    private ResponseEntity buildResponseEntity(Object body, HttpStatus status) {
        return new ResponseEntity<>(body, status);
    }

    protected ResponseEntity buildResponseEntityNoContent() {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
