package com.microservicios.commos.controller;

import com.microservicios.commos.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class CommonController<E, S extends CommonService<E>> extends AbstractController {

    @Autowired
    protected S service;

    @GetMapping
    public ResponseEntity<List<E>> listar() {
        return createOkStatusResponseEntity(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<E> findById(@PathVariable Long id) {
        return createOkStatusResponseEntity(service.findById(id));
    }

    @PostMapping
    public ResponseEntity<E> save(@RequestBody E entity) {
        return createOkStatusResponseEntity(service.save(entity));
    }

    @PutMapping
    public ResponseEntity<E> update(@RequestBody E entity) {
        return createOkStatusResponseEntity(service.save(entity));
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@PathVariable Long id) {
        service.deleteById(id);
        return buildResponseEntityNoContent();
    }

    @GetMapping("/paginado")
    public ResponseEntity<List<E>> listar(Pageable pageable) {
        return createOkStatusResponseEntity(service.findAll(pageable));
    }


}
