package com.microservicios.respuestas.service;

import com.microservicios.respuestas.entity.Respuesta;

public interface RespuestaService {

    Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas);

    Iterable<Respuesta> findAllByAlumnoByExamen(Long alumnoId, Long examenId);

    Iterable<Long> findExamenesByAlumno(Long alumnoId);
}
