package com.microservicios.respuestas.service.impl;

import com.microservicios.respuestas.entity.Respuesta;
import com.microservicios.respuestas.repository.RespuestaRepository;
import com.microservicios.respuestas.service.RespuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RespuestaServiceImpl implements RespuestaService {

    @Autowired
    private RespuestaRepository repository;

    @Override
    @Transactional
    public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas) {
        return repository.saveAll(respuestas);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Respuesta> findAllByAlumnoByExamen(Long alumnoId, Long examenId) {
        return repository.findAllByAlumnoByExamen(alumnoId, examenId);
    }


    @Override
    @Transactional(readOnly = true)
    public Iterable<Long> findExamenesByAlumno(Long alumnoId) {
        return repository.findExamenesByAlumno(alumnoId);
    }
}
