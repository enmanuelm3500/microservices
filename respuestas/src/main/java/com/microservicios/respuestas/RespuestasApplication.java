package com.microservicios.respuestas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@EntityScan({"com.microservicios.respuestas.entity",
        "com.microservices.common.examen.entity",
        "com.microservicios.common.alumno.alumno.entity"})
@SpringBootApplication
public class RespuestasApplication {

    public static void main(String[] args) {
        SpringApplication.run(RespuestasApplication.class, args);
    }

}
