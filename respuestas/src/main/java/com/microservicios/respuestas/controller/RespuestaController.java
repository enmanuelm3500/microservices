package com.microservicios.respuestas.controller;


import com.microservicios.respuestas.entity.Respuesta;
import com.microservicios.respuestas.service.RespuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RespuestaController {

    @Autowired
    private RespuestaService service;


    @PostMapping
    public ResponseEntity<Iterable<Respuesta>> saveAll(@RequestBody Iterable<Respuesta> respuestas){
        return new ResponseEntity<>(service.saveAll(respuestas), HttpStatus.OK);
    }

    @GetMapping("alumno/{alumnoId}/{examenId}")
    public ResponseEntity<Iterable<Respuesta>> findRespuestasByAlumnoByExamen(@PathVariable Long alumnoId, @PathVariable Long examenId ){
        return new ResponseEntity<>(service.findAllByAlumnoByExamen(alumnoId,examenId),HttpStatus.OK);
    }

    @GetMapping("alumno/{id}/examenes")
    public ResponseEntity<Iterable<Long>> findExamenesByAlumno(@PathVariable Long id){
        return new ResponseEntity<>(service.findExamenesByAlumno(id),HttpStatus.OK);
    }

}
