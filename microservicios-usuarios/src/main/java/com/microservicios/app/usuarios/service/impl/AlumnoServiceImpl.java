package com.microservicios.app.usuarios.service.impl;

import com.microservicios.app.usuarios.repository.AlumnoRepository;
import com.microservicios.app.usuarios.service.AlumnoService;
import com.microservicios.common.alumno.alumno.entity.Alumno;
import com.microservicios.commos.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AlumnoServiceImpl extends CommonServiceImpl<Alumno, AlumnoRepository> implements AlumnoService {

    @Override
    public List<Alumno> findByNombreOrApellido(String term) {
        return repository.findByNombreOrApellido(term);
    }
}
