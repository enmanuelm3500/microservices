package com.microservicios.app.usuarios.controller;

import com.microservicios.app.usuarios.service.AlumnoService;
import com.microservicios.common.alumno.alumno.entity.Alumno;
import com.microservicios.commos.controller.CommonController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class AlumnoController extends CommonController<Alumno, AlumnoService> {

    @GetMapping("/filtrar/{term}")
    public ResponseEntity<List<Alumno>> findById(@PathVariable String term){
        return createOkStatusResponseEntity(service.findByNombreOrApellido(term));
    }

    @PostMapping("/crear-alumno-foto")
    public ResponseEntity<Alumno> save(Alumno alumno, @RequestParam MultipartFile archivo) throws IOException {
        alumno.setFoto(archivo.getBytes());
        return createOkStatusResponseEntity(service.save(alumno));
    }



/*

    @Autowired
    private AlumnoService alumnoService;

    @GetMapping
    public ResponseEntity<List<Alumno>> listar(){
        return createOkStatusResponseEntity(alumnoService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Alumno> findById(@PathVariable Long id){
        return createOkStatusResponseEntity(alumnoService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Alumno> save(@RequestBody Alumno alumno){
        return createOkStatusResponseEntity(alumnoService.save(alumno));
    }

    @PutMapping
    public ResponseEntity<Alumno> update(@RequestBody Alumno alumno){
        return createOkStatusResponseEntity(alumnoService.save(alumno));
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@PathVariable Long id){
        alumnoService.deleteById(id);
        return buildResponseEntityNoContent();
    }
*/

}
