package com.microservicios.app.usuarios.service;

import com.microservicios.common.alumno.alumno.entity.Alumno;
import com.microservicios.commos.service.CommonService;

import java.util.List;

public interface AlumnoService extends CommonService<Alumno> {

    List<Alumno> findByNombreOrApellido(String term);

}
