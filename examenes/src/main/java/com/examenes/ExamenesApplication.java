package com.examenes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.microservices.common.examen.entity"})
public class ExamenesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamenesApplication.class, args);
    }

}
