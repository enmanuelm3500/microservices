package com.examenes.repository;

import com.microservices.common.examen.entity.Asignatura;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AsignaturaRepository extends PagingAndSortingRepository<Asignatura, Long> {
}
