package com.examenes.repository;

import com.microservices.common.examen.entity.Examen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ExamenRepository extends PagingAndSortingRepository<Examen, Long> {

    @Query("select a from Examen a where a.nombre like %?1%")
    List<Examen> findByNombre(String term);

}
