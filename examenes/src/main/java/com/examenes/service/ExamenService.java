package com.examenes.service;

import com.microservices.common.examen.entity.Asignatura;
import com.microservices.common.examen.entity.Examen;
import com.microservicios.commos.service.CommonService;

import java.util.List;

public interface ExamenService extends CommonService<Examen> {

    List<Examen> findByNombre(String term);

    List<Asignatura> findAllAsignaturas();
}
