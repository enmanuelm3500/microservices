package com.examenes.service.impl;

import com.examenes.repository.AsignaturaRepository;
import com.examenes.repository.ExamenRepository;
import com.examenes.service.ExamenService;
import com.microservices.common.examen.entity.Asignatura;
import com.microservices.common.examen.entity.Examen;
import com.microservicios.commos.service.impl.CommonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ExamenServiceImpl extends CommonServiceImpl<Examen, ExamenRepository> implements ExamenService {

    @Autowired
    private AsignaturaRepository asignaturaRepository;

    @Override
    @Transactional(readOnly = true)

    public List<Examen> findByNombre(String term) {
        return repository.findByNombre(term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Asignatura> findAllAsignaturas() {
        return (List) asignaturaRepository.findAll();
    }

}
