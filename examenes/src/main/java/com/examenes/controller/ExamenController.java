package com.examenes.controller;


import com.examenes.service.ExamenService;
import com.microservices.common.examen.entity.Asignatura;
import com.microservices.common.examen.entity.Examen;
import com.microservicios.commos.controller.CommonController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExamenController extends CommonController<Examen, ExamenService> {

    @GetMapping("/filtrar/{term}")
    public ResponseEntity<List<Examen>> findById(@PathVariable String term){
        return createOkStatusResponseEntity(service.findByNombre(term));
    }

    @GetMapping("/asignaturas")
    public ResponseEntity<List<Asignatura>> findAllAsignaturas(){
        return createOkStatusResponseEntity(service.findAllAsignaturas());
    }

}
